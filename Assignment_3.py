# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import matplotlib.pyplot as plt

#Define global variables
pa=1e5
pb=200.
f=1e-4
rho=1.
L=2.4e6
ymin=0.0
ymax=1e6

def extactansweru(y):
    'The exact answer of wind, u using an analytical method'
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def finitedifferencegrads(N):
    'Calculates pressure gradient, dpdy using the 2 point finite difference \
    method between postions 1 and N+1. Firstly it calculate the gradients \
    between N and N-1, then adds in the first and last points at 0 and \
    N using the input N and the position, y, the pressure, p the\
    interval length, deltay'
    
    #Intialise y
    y=np.linspace(ymin,ymax,N+1)
    #Define the pressure
    p = pa + pb * np.cos((np.pi * y) / L)  
    #define deltay
    deltay = (ymax - ymin) / N
    #Initialise dpdy
    dpdy = np.zeros(N+1)
    
    #calculate pressure gradients using the 2 point difference method
    for j in range (1,N):
        dpdy[j] = (p[j+1] - p[j-1]) / (2*deltay)  
   
    #Calculate the gradient at the first and last points
    dpdy[0] = (p[1]-p[0])/deltay
    dpdy[N] = (p[N]-p[N-1])/deltay
    
    return dpdy , y

def wind(dpdy):
    'Define the numerical solution of the wind in terms of pressure gradient'
    return -1/(rho*f) * dpdy 

def main():
    'Calculates the wind unsing the 2 point finite difference gradient \
    and compares it to the analytical solution graphically'
    
    #Define the number of intervals
    N=10
    #Calcualte the wind using dpdy and N
    fdg = finitedifferencegrads(N)
    u = wind(fdg[0])
    #Calculate the analitical solution/exact answer using y
    position = fdg[1]
    ue = extactansweru(position)
    
    #Plot the graph to compare, convert y from m to km
    plt.figure(1)
    plt.plot(position/1000, ue, label= 'Analytical Solution')
    plt.plot(position/1000, u, label= ' Numerical Solution')
    plt.title('Comparison of Numerical and Analytical')
    plt.legend(loc='best')
    plt.xlabel('y, km')
    plt.ylabel('u, m/s')
    plt.savefig('Solution Comparison.pdf')
    plt.show()    
    
    #Calculate the error
    error = np.zeros(N+1)
    for i in range (0,N+1):
        error[i] = abs(u[i]-ue[i])
        
    #Plot the error against postion    
    plt.figure(2)
    plt.plot(position/1000, error)
    plt.title('Error of the Numerical Solution')
    plt.xlabel('y, km')
    plt.ylabel('Error')
    plt.savefig('Error using Finite Difference Method.pdf')
    plt.show() 
        
main()

